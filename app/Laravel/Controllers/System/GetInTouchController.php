<?php namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Contact;
use App\Laravel\Models\GetInTouch;

/**
*
* Requests used for validating inputs
*/
// use App\Laravel\Requests\System\ContactRequest;


use Illuminate\Http\Request;
use App\Laravel\Requests\System\ContactRequest;
use App\Laravel\Requests\System\GetInTouchRequest;



/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class GetInTouchController extends Controller
{ 
    protected $data;


    public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	
		$this->data['heading'] = "Contacts";
	}

	public function index () {
		$this->data['page_title'] = " :: Get in Touch - Record Data";
		$this->data['touch'] = GetInTouch::orderBy('created_at',"DESC")->get();
		return view('system.touch.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Contacts - Add Contact";
		return view('system.contact.create',$this->data);
	}

	public function store(ContactRequest $request){

		try {
			// dd($request->all());
			$new_contacts = new Contact;
			$new_contacts->fill($request->only('description','phone','email','address'));
			

			if($new_contacts->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New Product has been added.");
				return redirect()->route('system.contact.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}
	public function store_getInTouch(GetInTouchRequest $request){

		try {
			// dd($request->all());
			$new_contacts = new GetInTouch;
			$new_contacts->fill($request->all());
			

			if($new_contacts->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New Product has been added.");
				return redirect()->route('system.contact.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}

	public function edit ($id = NULL) {

		

	$this->data['page_title'] = " :: Company Contacts - Edit Contacts";
		$edit_contacts = Contact::find($id);

		if (!$edit_contacts) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return view('system.contact.index',$this->data);
		}

		$this->data['edit_contacts'] = $edit_contacts;
		return view('system.contact.edit',$this->data);
	}

	public function update (ContactRequest $request, $id = NULL) {
		try {
			$new_contacts = Contact::find($id);

			if (!$new_contacts) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('system.contact.index');
			}
				$new_contacts->fill($request->all());

			if($new_contacts->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Branch Informations has been updated.");
				return redirect()->route('system.contact.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

public function destroy ($id = NULL) {
		try {
			$contacts = Contact::find($id);


			if (!$contacts) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.contact.index');
			}

			if($contacts->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.contact.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
