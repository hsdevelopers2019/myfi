<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Widget;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\WidgetRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, ImageUploader;

class WidgetController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Widget";
	}

	public function index () {
		$this->data['page_title'] = " :: Widget - Record Data";
		$this->data['widgets'] = Widget::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.widget.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Widget - Add new record";
		return view('system.widget.create',$this->data);
	}

	public function store (WidgetRequest $request) {
		try {
			$new_widget = new Widget;
        	$new_widget->fill($request->only('title','description', 'type'));
        	$new_widget->slug = str_slug($request->get('title'), '-');

			if($new_widget->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.widget.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Widget - Edit record";
		$widget = Widget::find($id);

		if (!$widget) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.widget.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.widget.index');	
		}

		$this->data['widget'] = $widget;
		return view('system.widget.edit',$this->data);
	}

	public function update (WidgetRequest $request, $id = NULL) {
		try {
			$widget = Widget::find($id);

			if (!$widget) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.widget.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.widget.index');	
			}

        	$widget->fill($request->only('title','description', 'type'));
        	$widget->slug = str_slug($request->get('title'), '-');

			if($widget->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.widget.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$widget = Widget::find($id);

			if (!$widget) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.widget.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.widget.index');	
			}

			if($widget->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.widget.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}