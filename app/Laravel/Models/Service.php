<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "services";
       
    protected $fillable = [
       'service_title' , 'content', 'excerpt'
        
    ];

    protected $hidden = [];

 }
