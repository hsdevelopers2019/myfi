<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "subscribers";
       
    protected $fillable = [
    	'email'
    ];

    protected $hidden = [];
}
