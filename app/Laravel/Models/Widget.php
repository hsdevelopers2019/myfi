<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth,Helper;

class Widget extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "widget";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','description', 'type'];

    protected $appends = ['excerpt'];

    public $timestamps = true;

    public function getExcerptAttribute(){
        return Helper::get_excerpt($this->description);
    }

    public function scopeKeyword($query, $keyword = NULL){
        if($keyword){
            $keyword = strtolower($keyword);
            return $query->whereRaw("LOWER(title) LIKE '{$keyword}%'")
                        ->orWhereRaw("LOWER(content) LIKE '{$keyword}%'");
        }
    }
}
