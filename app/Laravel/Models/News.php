<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth,Helper;

class News extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "news";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','content','video_url','video_source','category_id'];

    protected $appends = ['thumbnail','excerpt','new_directory'];

    public $timestamps = true;

    public function getNewDirectoryAttribute(){
        return str_replace(env("BLOB_STORAGE_URL"), env("CDN_STORAGE_URL"), $this->directory);
    }

    public function category(){
        return $this->hasOne('App\Laravel\Models\ArticleCategory','id','category_id');
    }

    public function getExcerptAttribute(){
        return Helper::get_excerpt($this->content);
    }

    public function getThumbnailAttribute(){
        if($this->filename){
            return "{$this->new_directory}/resized/{$this->filename}";
        }

        return asset('placeholder/user.jpg');
    }

    public function scopeKeyword($query, $keyword = NULL){
        if($keyword){
            $keyword = strtolower($keyword);
            return $query->whereRaw("LOWER(title) LIKE '{$keyword}%'")
                        ->orWhereRaw("LOWER(content) LIKE '{$keyword}%'");
        }
    }

    public function scopeCategories($query,$category_ids = NULL){
        if($category_ids){
            $categories = explode(",", $category_ids);
            return $query->whereIn('category_id',$categories);
        }
    }

}
