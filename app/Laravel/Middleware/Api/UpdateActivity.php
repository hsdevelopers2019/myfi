<?php

namespace App\Laravel\Middleware\Api;

use Carbon, Closure, DB, Helper,Str;
use App\Laravel\Events\UserAction;


class UpdateActivity
{
   /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$user = $request->user();
        event( new UserAction($user, ['use_app']) );
    	return $next($request);
    }
}