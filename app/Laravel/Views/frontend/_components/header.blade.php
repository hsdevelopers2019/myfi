

        <nav class="navbar navbar-default" style="position: sticky;">
            <div class="container" id="top">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-nav-demo" aria-expanded="false" style="margin-left: 20px;margin-bottom: 10px;">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a href="{{ route('frontend.homepage') }}" class="navbar-brand img-responsive"><span class="default-logo img-responsive"><img src="{{ asset('frontend/assets/img/logo.png') }}" alt="logo" style="margin-top: -15px;margin-left: -30px;width: 300px;"></span></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-nav-demo" style="margin-top: 20px;">
                    <ul class="nav navbar-nav text-center navbar-right" >
                    <li><a class="nav-link" href="{{ route('frontend.homepage') }}">Home</a></li> 
                    <li><a class="nav-link" href="{{ route('frontend.about') }}">About</a></li>
                    <li><a class="nav-link" href="{{ route('frontend.opportunities') }}">Opportunities</a></li>
                    <li><a class="nav-link" href="{{ route('frontend.service') }}">What We Do</a></li>
                    <li><a class="nav-link" href="{{ route('frontend.gallery') }}">Gallery</a></li>
                    <li><a class="nav-link" href="{{ route('frontend.contact') }}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </nav>

