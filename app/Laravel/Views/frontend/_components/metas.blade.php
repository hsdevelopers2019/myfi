    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Karnes Business Template" />
    <meta name="keywords" content="Finance, Landing, Business, Onepage, Html, Business" />

    <!--====== TITLE TAG ======-->
    <title>Human Capital Asia Inc.</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="{{ asset('frontend/assets/img/logo_icon.png') }}" />

    <!--====== STYLESHEETS ======-->
    <link href="{{ asset('frontend/assets/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/icons.css') }}" rel="stylesheet">

    <!--====== MAIN STYLESHEETS ======-->
    <link href="{{ asset('frontend/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/css/responsive.css') }}" rel="stylesheet">
    <script src=" {{ asset('frontend/assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
