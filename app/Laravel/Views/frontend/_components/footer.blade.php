 <footer class="footer-area sky-gray-bg relative" style="padding-bottom: 30px;">
        <div class="footer-top-area padding-50-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-12 col-xs-12 sm-center xs-center">
                        <div class="single-footer-widgets">
                            <div class="footer-logo">
                                <a href="{{ route('frontend.homepage') }}"><img src="{{ asset('frontend/assets/img/full.png') }}" alt="" width="150px;"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3  col-md-offset-1 col-sm-4 col-xs-12">
                        <div class="single-footer-widgets mb30">
                            <h4>QUICK LINKS</h4>
                            <ul>
                                <li><a href="{{ route('frontend.homepage') }}">HOME</a></li>
                                <li><a href="{{ route('frontend.about') }}">ABOUT US</a></li>
                                <li><a href="{{ route('frontend.opportunities') }}">OPPORTUNITIES</a></li>
                                <li><a href="{{ route('frontend.service') }}">WHAT WE DO</a></li>
                                <li><a href="{{ route('frontend.contact') }}">CONTACT US</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="single-footer-widgets">
                            <h4>CONTACT US</h4>
                           @forelse ($contacts as $contact)
                                <p>Human Capital Asia Inc <br>{{ $contact->address }}</p>
                                <p><i class="fa fa-phone"></i> : {{ $contact->phone }} </p>
                                <p><i class="fa fa-envelope-o"></i> : {{ $contact->email }} </p>
                               @empty
                               <p>Human Capital Asia Inc <br>301 Jollibee Center San Miguel Ave. Pasig City</p>
                                <p><i class="fa fa-phone"></i> : Please Enter Phone number here</p>
                                <p><i class="fa fa-envelope-o"></i> : Please Enter Email Desription Here</p>
                           @endforelse   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area">

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                        <div class="footer-social-bookmark text-center wow fadeIn">
                            <ul class="social-bookmark">
                                <li><a href="https://www.facebook.com/pages/Human-Capital-Asiainc/248589775328463?ref=br_rs"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/hcatweets"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            <p>Copyright &copy;</a> All Right Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--FOOER AREA END-->

   