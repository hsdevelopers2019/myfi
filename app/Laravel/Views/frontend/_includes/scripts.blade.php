


    <script src="{{ asset('frontend/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/jquery.appear.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/stellar.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/jquery-modal-video.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/venobox.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/placeholdem.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/contact-form.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/app.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/accordion.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/tilt.jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/waypoints.min.js') }}"></script>    
    <script src="{{ asset('frontend/assets/js/main.js') }}"></script>
    <script src="https://kit.fontawesome.com/3aefd7621a.js"></script>


@yield('page-scripts')

</body>