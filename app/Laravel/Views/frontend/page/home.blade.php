<!DOCTYPE html>
<html lang="zxx">
    <head>
        <!-- Metas -->
        <meta charset="utf-8">
        <!-- Title  -->
        <title>MYFI</title>
         <link rel="icon" type="image/gif" href="assets/img/logo.png" />
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">
        <!-- Plugins -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/plugins.css') }}" />
        <!-- Core Style Css -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}" />
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <!-- =====================================
        ==== Start Loading -->
        <div class="loading">
            <div class="text-center middle">
                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
        
        <!-- End Loading ====
        ======================================= -->
        
        <!-- =====================================
        ==== Start Navbar -->
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <!-- Logo -->
                <a class="logo" href="#">
                    <img src="{{ asset('assets/frontend/img/logow.png') }}" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"><i class="fas fa-bars"></i></span>
                </button>
                <!-- navbar links -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" data-scroll-nav="0">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="1">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="2">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="3">Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="6">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar ====
        ======================================= -->
        <!-- =====================================
        ==== Start Header -->
        <header class="header valign bg-img" data-scroll-index="0" data-overlay-dark="5" data-background="{{ asset('assets/frontend/img/bl.jpg') }}" data-stellar-background-ratio="0.5">
            <!-- particles -->
            <div id="particles-js"></div>
            <div class="container">
                <div class="row">
                    <div class="full-width text-center caption mt-30">
                        <h1><span>BRIDGING THE FUTURE</span></h1>
                        <p>MYFI provides high quality connectivity and various digital services.</p>
                        <a class="butn butn-light mt-30" href="#" data-scroll-nav="6" >
                            <span>Learn More</span>
                        </a>
                        
                    </div>
                </div>
            </div>
        </header>
        <!-- End Header ====
        ======================================= -->
        <!-- =====================================
        ==== Start Hero -->
        <section class="hero section-padding" data-scroll-index="1">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="intro offset-lg-1 col-lg-10 text-center mb-80">
                        <h2>WE ARE <span style="color:#2c8ede;">MYFI</span></h2>
                        <h3>Who We Are And What Can We do?</h3>
                        <h4<p><span style="color:#2c8ede;">MYFI</span> is an ISP/VAS Company dedicated to provide high quality connectivity and innovate software related services.</p></h4>
                    </div>
                    <div class="col-lg-6">
                        <div class="item text-center mb-md50">
                            <span class="icon"><i class="fa fa-wifi"></i></span>
                            <h5><span style="color:#2c8ede;">MYFI CONNECTS</span></h5>
                            <p>MYFI offers a robust network connectivity that is secure and scalable. Whether you're mid-size business or an enterprise. It is customized to meet your needs.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="item text-center">
                            <span class="icon"><i class="fa fa-connectdevelop"></i></span>
                            <h5><span style="color:#2c8ede;">MYFI DEVELOPS</span></h5>
                            <p>MYFI offers a wide range of digital services to transform your business. Whether if it is to advertise your brand or create custom solution.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Hero ====
        ======================================= -->
        <!-- =====================================
        ==== Start Numbers -->
        <div class="numbers section-padding bg-img bg-fixed"  data-overlay-dark="8" data-background="{{ asset('assets/frontend/img/bg3.jpg') }}">
            <div class="container">
                 
            </div>
        </div>
        <!-- End Numbers ====
        ======================================= -->
        
        <!-- =====================================
        ==== Start Services -->
        <section class="services section-padding" data-scroll-index="2">
            <div class="container">
                <div class="row">
                    <div class="section-head text-center col-sm-12">
                        <h4>OUR <span style="color:#2c8ede;">SERVICES</span></h4>
                        <h6>Awesome Featruse</h6>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon"><i class="fa fa-keyboard-o"></i></span>
                            <h6>LGU DISPATCH</h6>
                            <p>Myfi promotes efficient city management through an LGU emergency dispatch platform.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 bord">
                        <div class="item mb-md50">
                            <span class="icon"><i class="fa fa-wifi"></i></span>
                            <h6>ISP/VAS</h6>
                            <p>Myfi offers the most basic connectivity needs (fiber or coper) to the most advanced solutions in the mar\ket.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon"><i class="fa fa-university" aria-hidden="true"></i></span>
                            <h6>COMMAND CENTERS</h6>
                            <p>Myfi offers an all in one command center to help manage city task and emergency dispatch.</p>
                        </div>
                    </div>
                    <hr>
                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon"><i class="fa fa-connectdevelop"></i></span>
                            <h6>SOFTWARE DEVELOPMENT</h6>
                            <p>Myfi can build custom digital solutions. May it be a website or an application.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 bord">
                        <div class="item mb-sm50">
                            <span class="icon"><i class="fa fa-cogs"></i></span>
                            <h6>CUSTOMER SUPPORT</h6>
                            <p>Myfi provides fast and reliable customer support, our team can support you to solve your inquiries.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="item">
                            <span class="icon"><i class="fa fa-paint-brush"></i></span>
                            <h6>MULTIMEDIA DESIGN</h6>
                            <p>Myfi can create great designs, to be used online ads and various materials.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Services ====
        ======================================= -->
        <!-- =====================================
        ==== Start Numbers -->
        <div class="numbers section-padding bg-img bg-fixed" data-scroll-index="2" data-overlay-dark="8" data-background="{{ asset('assets/frontend/img/bgg.jpg') }}">
            <div class="container">
                <img src="{{ asset('assets/frontend/img/wiw.png') }}" alt="logo">
            </div>
        </div>
        <!-- End Numbers ====
        ======================================= -->
        <!-- =====================================
        ==== Start Works -->
        <section class="works section-padding" data-scroll-index="3">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="section-head text-center col-sm-12">
                        <h4>OUR <span style="color:#2c8ede;">PROJECTS</span></h4>
                        <h6>Latest Projects</h6>
                    </div>
                    <!-- filter links -->
                    
                    <div class="clearfix"></div>
                    <!-- gallery -->
                    <div class="gallery text-center full-width">
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items graphic">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/9.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        
                                        
                                        <a href="{{ asset('assets/frontend/img/portfolio/9.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items web">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/10.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        
                                        
                                        <a href="{{ asset('assets/frontend/img/portfolio/10.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items brand">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/11.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        
                                        
                                        <a href="{{ asset('assets/frontend/img/portfolio/11.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items graphic">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/12.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <a href="{{ asset('assets/frontend/img/portfolio/12.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items web">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/13.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        
                                        
                                        <a href="{{ asset('assets/frontend/img/portfolio/13.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items brand">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/14.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <a href="{{ asset('assets/frontend/img/portfolio/14.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items brand">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/15.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        
                                        
                                        <a href="{{ asset('assets/frontend/img/portfolio/15.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items graphic">
                            <div class="item-img">
                                <img src="{{ asset('assets/frontend/img/portfolio/16.jpg') }}" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        
                                        
                                        <a href="{{ asset('assets/frontend/img/portfolio/16.jpg') }}" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Works ====
        ======================================= -->
        
        <!-- =====================================
        ==== Start Teams -->
        <section class="team section-padding">
            <div class="container">
                <div class="row">
                    <div class="section-head text-center col-sm-12">
                        <h4>OUR <span style="color:#2c8ede;">TEAM</span></h4>
                        <h6>The Talent Crew</h6>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="item mb-50">
                            <div class="team-img">
                                <img src="{{ asset('assets/frontend/img/team/1.jpg') }}" alt="">
                            </div>
                            <div class="info">
                                <h6>Alex Smith</h6>
                                <span>Project Manager</span>
                                <p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
                                <div class="social">
                                    <a href="#0" class="icon">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-behance"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item mb-50">
                            <div class="team-img">
                                <img src="{{ asset('assets/frontend/img/team/2.jpg') }}" alt="">
                            </div>
                            <div class="info">
                                <h6>Alex Smith</h6>
                                <span>Project Manager</span>
                                <p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
                                <div class="social">
                                    <a href="#0" class="icon">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-behance"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item mb-md50">
                            <div class="team-img">
                                <img src="{{ asset('assets/frontend/img/team/3.jpg') }}" alt="">
                            </div>
                            <div class="info">
                                <h6>Alex Smith</h6>
                                <span>Project Manager</span>
                                <p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
                                <div class="social">
                                    <a href="#0" class="icon">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-behance"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item">
                            <div class="team-img">
                                <img src="{{ asset('assets/frontend/img/team/4.jpg') }}" alt="">
                            </div>
                            <div class="info">
                                <h6>Alex Smith</h6>
                                <span>Project Manager</span>
                                <p>Lorem Ipsum is simply dummy text of the printing and type setting industry.</p>
                                <div class="social">
                                    <a href="#0" class="icon">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-behance"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Teams ====
        ======================================= -->
        
        
        
        
        <!-- =====================================
        ==== Start Contact-Info -->
        <div class="contact-info section-box" data-scroll-index="6">
            <div class="container-fluid">
                <div class="row">
                    
                    <!-- Contact Info -->
                    <div class="info bg-img col-md-12" data-overlay-dark="8" data-background="{{ asset('assets/frontend/img/bg3.jpg') }}">
                        <div class="gradient row">
                            <div class="item col-4">
                                <span class="icon icon-basic-smartphone"></span>
                                <div class="cont">
                                    <h6>Phone : </h6>
                                    <p>+63 9507817064</p>
                                </div>
                            </div>
                            <div class="item col-4">
                                <span class="icon icon-basic-geolocalize-05"></span>
                                <div class="cont">
                                    <h6>Address : </h6>
                                    <p>Brgy. 123 - hayahay Street 10/555</p>
                                    <p>25th Avenue, Upper Left Side, Mandaluyong City</p>
                                </div>
                            </div>
                            <div class="item col-4">
                                <span class="icon icon-basic-mail"></span>
                                <div class="cont">
                                    <h6>Email : </h6>
                                    <p>myfiph@gmail.com</p>
                                    <p>myfi@gmail.com</p>
                                </div>
                            </div>
                            <div class="social col-12 d-flex justify-content-center">
                                <a href="#0" class="icon">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#0" class="icon">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#0" class="icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#0" class="icon">
                                    <i class="fab fa-behance"></i>
                                </a>
                                <a href="#0" class="icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- The Map -->
                    
                </div>
            </div>
        </div>
        <!-- End Contact-Info ====
        ======================================= -->
        <!-- =====================================
        ==== Start Contact -->
        <section class="contact section-padding" data-scroll-index="6">
            <div class="container">
                <div class="row">
                    
                    <div class="section-head text-center col-sm-12">
                        <h4>Get In Touch</h4>
                        <h6>Feel Free To Contact Us</h6>
                    </div>
                    <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10">
                        <form class="form" id="contact-form" method="post" action="http://www.myfi.com/contact.php">
                            <div class="messages"></div>
                            <div class="controls">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_name" type="text" name="name" placeholder="Name" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="form_subject" type="text" name="subject" placeholder="Subject">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea id="form_message" name="message" placeholder="Message" rows="4" required="required"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="submit"><span>Send Message</span></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Contact ====
        ======================================= -->
        <!-- =====================================
        ==== Start Footer -->
        <footer class="text-center">
            <div class="container">
                <!-- Logo -->
                <a class="logo" href="#">
                    <img src="{{ asset('assets/frontend/img/myfiw.png') }}" alt="logo">
                </a><br>
                
                <div class="social">
                    <a href="#0" class="icon">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#0" class="icon">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#0" class="icon">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                    <a href="#0" class="icon">
                        <i class="fab fa-behance"></i>
                    </a>
                    <a href="#0" class="icon">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
                <p>&copy;MYFI 2019. All Rights Reserved.</p>
            </div>
        </footer>
        <!-- End Footer ====
        ======================================= -->
        
        <!-- jQuery -->
        <script src="{{ asset('assets/frontend/js/jquery-3.0.0.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/jquery-migrate-3.0.0.min.js') }}"></script>
        <!-- popper.min -->
        <script src="{{ asset('assets/frontend/js/popper.min.js') }}"></script>
        <!-- bootstrap -->
        <script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
        <!-- scrollIt -->
        <script src="{{ asset('assets/frontend/js/scrollIt.min.js') }}"></script>
        <!-- jquery.waypoints.min -->
        <script src="{{ asset('assets/frontend/js/jquery.waypoints.min.js') }}"></script>
        <!-- jquery.counterup.min -->
        <script src="{{ asset('assets/frontend/js/jquery.counterup.min.js') }}"></script>
        <!-- owl carousel -->
        <script src="{{ asset('assets/frontend/js/owl.carousel.min.js') }}"></script>
        <!-- jquery.magnific-popup js -->
        <script src="{{ asset('assets/frontend/js/jquery.magnific-popup.min.js') }}"></script>
        <!-- stellar js -->
        <script src="{{ asset('assets/frontend/js/jquery.stellar.min.js') }}"></script>
        <!-- isotope.pkgd.min js -->
        <script src="{{ asset('assets/frontend/js/isotope.pkgd.min.js') }}"></script>
        <!-- YouTubePopUp.jquery -->
        <script src="{{ asset('assets/frontend/js/YouTubePopUp.jquery.js') }}"></script>
        <!-- particles.min js -->
        <script src="{{ asset('assets/frontend/js/particles.min.js') }}"></script>
        <!-- app js -->
        <script src="{{ asset('assets/frontend/js/app.js') }}"></script>
        <!-- Map -->
        <script src="{{ asset('assets/frontend/js/map.js') }}"></script>
        <!-- validator js -->
        <script src="{{ asset('assets/frontend/js/validator.js') }}"></script>
        <!-- custom scripts -->
        <script src="{{ asset('assets/frontend/js/scripts.js') }}"></script>
        <!-- google map api -->
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK7lXLHQgaGdP3IvMPi1ej0B9JHUbcqB0&amp;callback=initMap">
        </script>
    </body>
    <!-- Mirrored from www.innovationplans.com/idesign/arco/particles.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Oct 2019 02:45:37 GMT -->
</html>