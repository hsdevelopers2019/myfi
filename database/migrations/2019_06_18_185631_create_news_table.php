<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('news', function(Blueprint $table){
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned()->default("0");
            $table->string('title')->nullable();
            $table->integer('category_id')->default("0");
            $table->text('video_url')->nullable();
            $table->string('video_source')->nullable();
            $table->text('content')->nullable();

            $table->text('directory')->nullable();
            $table->string('path')->nullable();
            $table->string('filename')->nullable();

            $table->string('is_approved')->nullable()->default("pending");
            $table->string('status')->nullable()->default("draft");
            $table->string('source')->nullable()->default("file");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('news');
    }
}
